package pl.kasprowski.msscbeerservice.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import pl.kasprowski.msscbeerservice.web.model.BeerDto;
import pl.kasprowski.msscbeerservice.web.model.BeerStyle;

import java.math.BigDecimal;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(BeerController.class)
class BeerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Test
    void getBeerById() throws Exception {
        mockMvc.perform(get("/api/v1/beer/" + UUID.randomUUID())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void saveBeer() throws Exception {
        final BeerDto dto = BeerDto.builder()
                .beerName("name")
                .beerStyle(BeerStyle.ALE)
                .upc(1L)
                .price(new BigDecimal("10.01"))
                .build();
        final String json = mapper.writeValueAsString(dto);

        mockMvc.perform(post("/api/v1/beer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
    }

    @Test
    void updateBeer() throws Exception {
        final BeerDto dto = BeerDto.builder()
                .beerName("name")
                .beerStyle(BeerStyle.ALE)
                .upc(1L)
                .price(new BigDecimal("10.01"))
                .build();
        final String json = mapper.writeValueAsString(dto);

        mockMvc.perform(put("/api/v1/beer/" + UUID.randomUUID())
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isNoContent());
    }

    @Test
    void shouldFailOnInvalidRequest() throws Exception {
        final BeerDto dto = BeerDto.builder()
                .upc(1L)
                .price(new BigDecimal("10.01"))
                .build();
        final String json = mapper.writeValueAsString(dto);

        final MvcResult response = mockMvc.perform(put("/api/v1/beer/" + UUID.randomUUID())
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest())
                .andReturn();

        final String responseBody = response.getResponse().getContentAsString();
        assertThat(responseBody).isNotEmpty();
        assertThat(responseBody).contains("must not be null", "must not be blank");
    }
}