package pl.kasprowski.msscbeerservice.web.model;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;

public class BeerPageList extends PageImpl<BeerDto> {
    public BeerPageList(final List<BeerDto> content, final Pageable pageable, final long total) {
        super(content, pageable, total);
    }

    public BeerPageList(final List<BeerDto> content) {
        super(content);
    }
}
