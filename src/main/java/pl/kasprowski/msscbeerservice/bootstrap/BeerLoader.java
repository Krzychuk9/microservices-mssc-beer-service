package pl.kasprowski.msscbeerservice.bootstrap;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.kasprowski.msscbeerservice.domain.Beer;
import pl.kasprowski.msscbeerservice.repository.BeerRepository;

import java.math.BigDecimal;

@Component
@Slf4j
public class BeerLoader implements CommandLineRunner {

    private final BeerRepository repository;

    public BeerLoader(final BeerRepository repository) {
        this.repository = repository;
    }

    @Override
    public void run(final String... args) throws Exception {
        loadBeer();
    }

    private void loadBeer() {
        if (repository.count() == 0) {
            repository.save(Beer.builder()
                    .beerName("Mango Bobs")
                    .beerStyle("IPA")
                    .quantityToBrew(200)
                    .minOnHAnd(12)
                    .upc(33701000000L)
                    .price(new BigDecimal("12.96"))
                    .build());

            repository.save(Beer.builder()
                    .beerName("Galaxy Cat")
                    .beerStyle("PALE_ALE")
                    .quantityToBrew(200)
                    .minOnHAnd(12)
                    .upc(337010000123L)
                    .price(new BigDecimal("11.11"))
                    .build());
        }

        log.info("Loaded beers: {}", repository.count());
    }
}
